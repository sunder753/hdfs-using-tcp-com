# HDFS using TCP com

A sample prroject to acheive initeerraction betwee HDFS and non-HDFS systems using TCP communication. 


SAMPLE  PROJECT :
=================
How to implement an API gateway to be an intermediary between a very basic 
companion JAVA program and HADOOP.

GETTING STARTED
===============

PREREQISITIES
==============

Used Eclispe IDE For Developmet-with  Less than java-1.7 version


Hadoop 2.6.0-pseudo distrubuted cluster =installed on vmware (which acts as Sever)

windows pc (which acts as client)



Steps to Execute This project:
===============================

step:1

Install hadoop in your local system
 please go through the Intallation readme file to set up hadoop cluster on virtual machine

step:2

clone/ download the project to your local machine.

Extract the zip file


step:3
[NOTE: Before building to jar you need to set your hdfs file system path manully parameter in LOadConfigurations.java]
which is present in the core-site.xml
        <configuration>
  <property>
  	 <name>fs.defaultFS</name>
  	<value>hdfs://localhost:8020</value><!-- my hdfs path-->
  </property>
  </configuration>
                    


	>>first pick up TCPClient.java program copy it on your widows machine

	>>set up the downloaded project to eclipse
	
	
	
	>>build a jar file from eclipse (.jar)

	>>copy the (.jar) from windows machine to linux which is upend running on vmwaree.

	>>
step:4 
	>>open terminal on your linux VMmachine 
	
	>>go to the folder where you copied the .jar file using linux commands
	
step:5
	>> we must and shoould start the daemons of hadoop.

	>>to start deamons pleasee enteer the following command 
	
			>>> start-all.sh

	>>>deamons will geet started now and to check whether they are started or not
	
	>>type the following command
		
			>>> jps
	
step:6
	>>run the following command to execute the required task

	>>  yarn jar [yourjar.jar] com/hdfs/App

	>>now the TCPServer will be started and will be up end running
	
	>>simultaneoulsly go to your windows machine and complile the TCPClient.java code
	and extecute it as follows.
			>>javac TCPClient.java
			>>java	TCPCliet
	>>then it will promt you to enter the ipaddress of linuxmachine at where you
		server program is running. Now the handshacking pracess will start 
	between those to sockets. 

step:7
	>> based on the prompts or  produced from both sockets we can proceed 
	to perform hdfs operations based on java API