package com.hdfs.tail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Tail extends LoadConfigurations implements Command {

	private RandomAccessFile randomAccessFile;

	@Override
	public void run() {

	
		
		    String filePath = null;
			File file = new File(hdfs);
		    StringBuilder builder = new StringBuilder();
		    try {
		        randomAccessFile = new RandomAccessFile(filePath, "r");
		        long pos = file.length() - 1;
		        randomAccessFile.seek(pos);

		        for (long i = pos - 1; i >= 0; i--) {
		            randomAccessFile.seek(i);
		            char c = (char) randomAccessFile.read();
		            if (c == '\n') {
		                int n = 0;
						n--;
		                if (n == 0) {
		                    break;
		                }
		            }
		            builder.append(c);
		        }
		        builder.reverse();
		        System.out.println(builder.toString());
		    } catch (FileNotFoundException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
	}


