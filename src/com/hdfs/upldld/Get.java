package com.hdfs.upldld;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Get  extends LoadConfigurations implements Command {

	@Override
	public void run() {
		
		
		
		//local path to put file
		System.out.println("enter local file system path url");
		String localPath=s.next();
		
		
		
		//hdfs destination to store
		System.out.println("enter hdfs url");
		String hdfsDir=hdfs+s.next();
		
		// Get the instance of the HDFS 
		FileSystem fs = null;
		try {
			fs = FileSystem.get(URI.create(hdfs),conf);
		} catch (IOException e1) {
			System.out.println("enter valid   url");

			//			e1.printStackTrace();
		}
		
		try {
			fs.copyToLocalFile(new Path(hdfsDir), new Path(localPath));
		} catch (IOException e) {
			System.out.println("enter valid details");
			//e.printStackTrace();
		}
		
	}

}
