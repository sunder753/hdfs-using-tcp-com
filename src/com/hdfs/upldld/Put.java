package com.hdfs.upldld;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Put  extends LoadConfigurations implements Command {

	@Override
	public void run() {

		
		//local path to put file
		System.out.println("enter local path url");
		String localpath=s.next();
		
		
		
		//hdfs destination to store
		System.out.println("enter hdfs url");
		String hdfsDir=hdfs+s.next();
		
		
		// Get the instance of the HDFS 
		FileSystem fs = null;
		try {
			fs = FileSystem.get(URI.create(hdfs),conf);
		} catch (IOException e) {
			System.out.println("enter valid   url");

			//e.printStackTrace();
		}
		try {
			fs.copyFromLocalFile(new Path(localpath),new Path(hdfsDir));
		} catch (IOException e) {
			System.out.println("enter valid details");
			//e.printStackTrace();
		}
	
		
	}

}
