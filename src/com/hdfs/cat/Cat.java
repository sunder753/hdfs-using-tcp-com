package com.hdfs.cat;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Cat  extends LoadConfigurations implements Command {

	@Override
	public  void run() {
		
		 // URI of the file to be read
		  System.out.println("please enter your path to hdfs file you wna to read");
		  String file=s.next();
		  URI uri = null;
		try {
			uri = new URI(hdfs+file);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		 
		FileSystem fs=null;
		// Get the instance of the HDFS 
		try {
			fs = FileSystem.get(uri, conf);
		} catch (IOException e) {
			System.out.println("file not found please enter a valid file and try again with cmmand");
		
		}
		  
		// A reference to hold the InputStream
		  InputStream inputStream = null;
		  try{
		  // Prepare the Path, i.e similar to File class in Java, Path represents file in HDFS
		   Path path = new Path(uri);
		   // Open a Input Stream to read the data from HDFS
		   inputStream = fs.open(path);
		   // Use the IOUtils to flush the data from the file to console
		   IOUtils.copyBytes(inputStream, System.out, 4096, false);
		  } catch (Exception e) {
				System.out.println("file not found please enter a valid file and try again with cmmand");

			  //e.printStackTrace();
		}finally{
		   // Close the InputStream once the data is read
		   IOUtils.closeStream(inputStream);
		   
		  }
		
	}
}