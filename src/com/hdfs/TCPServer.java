package com.hdfs;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import com.hdfs.cat.Cat;
import com.hdfs.copyfromTolocal.CopyFromLocal;
import com.hdfs.copyfromTolocal.CopyToLocal;
import com.hdfs.cp.Cp;
import com.hdfs.ls.Ls;
import com.hdfs.mkdir.Mkdir;
import com.hdfs.mv.Mv;
import com.hdfs.rm.Rm;
import com.hdfs.tail.Tail;
import com.hdfs.upldld.Get;
import com.hdfs.upldld.Put;


class TCPServer {
 public void runServer() throws Exception {
  String clientSentence;
  String capitalizedSentence;
  ServerSocket welcomeSocket = new ServerSocket(6789);
  System.out.println("Server Started ");

  Command comm = null;
  while (true) {
   Socket connectionSocket = welcomeSocket.accept();
   System.out.println("Connected to "+connectionSocket.getInetAddress());
   BufferedReader inFromClient =
    new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
   DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
   clientSentence = inFromClient.readLine();
   System.out.println("Received commad to execute is : " + clientSentence);
   capitalizedSentence = clientSentence.toUpperCase() + '\n';
   System.out.println("sending acknowldgement to client");
   outToClient.writeBytes(capitalizedSentence);

   comm = getInstance(clientSentence.toUpperCase());
   if(null != comm) {
	   comm.run();
	   System.out.println("command executed");
	
   }else {	   
	   outToClient.writeBytes("Command executed is not supported.");
   }
  }
 }
 
 private Command getInstance(String command) {
	 switch (command) {
	case "LS":
		return new Ls();
	case "MKDIR":
		return new Mkdir();
	case "CAT":
		return new Cat();
	case "CP":
		return new Cp();
	case "MV":
		return new Mv();
	case "RM":
		return new Rm();
	case "GET":
		return new Get();
	case "PUT":
		return new Put();
	case "COPYTOLOCAL":
		return new CopyToLocal();
	case "COPYFROMLOCAL":
		return new CopyFromLocal();
	case "TAILL":
		return new Tail();
	default:
		return null;
	}
 }
}
