package com.hdfs.mkdir;


import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Mkdir extends LoadConfigurations implements Command {
	 
	public void run() {

		

		 // Get the instance of Configuration
		 Configuration conf = new Configuration();

		 // URI of the Folder to be Created
		 System.out.println("enter folder name to create");
		 String directory = hdfs +s.next();
		

	  // Get FileSystem Instance for given uri 
	  FileSystem fs = null;
	try {
		fs = FileSystem.get(URI.create(hdfs), conf);
	} catch (IOException e) {
		System.out.println("enter valid   url");

		//e.printStackTrace();
	}
		
		boolean isCreated = false;
	try {
		isCreated = fs.mkdirs(new Path(directory));
	} catch (IOException e) {
		e.printStackTrace();
	}
	  if (isCreated) {
	   System.out.println("Directory created");
	  } else {
	   System.out.println("Directory creation failed");
	  }
	 }

	
	
		
	}


