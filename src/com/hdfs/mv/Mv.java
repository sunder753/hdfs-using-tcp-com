package com.hdfs.mv;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Mv  extends LoadConfigurations implements Command {

	@Override
	public void run() {
	
		
	
		//local path to put file
		System.out.println("enter hdfs  file  you want to move");
		String hdfsSrc=hdfs+s.next();
		
		
		
		//hdfs destination to store
		System.out.println("enter hdfs destination dir  to paste url");
		String hdfsDes=hdfs+s.next();
		
		
		// Get the instance of the HDFS 
		FileSystem fs = null;
		try {
			fs = FileSystem.get(URI.create(hdfs),conf);
		} catch (IOException e1) {
			System.out.println("enter valid  hdfs url");

			//e1.printStackTrace();
		}
		
		// Using the rename to move the file from hdfs source to hdfs destiation
		try {
			fs.rename(new Path(hdfsSrc), new Path(hdfsDes));
		} catch (IOException e) {
			System.out.println("enter valid details");
			//e.printStackTrace();
		}
		
	}

}
