package com.hdfs.copyfromTolocal;


import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class CopyFromLocal extends LoadConfigurations implements Command {
	

		public  void run()  {
			//local path to put file
			System.out.println("enter local path url");
			String localpath=s.next();
			
			
			
			//hdfs destination to store
			System.out.println("enter hdfs url");
			String hdfsDir=hdfs+s.next();
			        
			
			       FileSystem fs = null;
				try {
					fs = FileSystem.get(conf);
				} catch (IOException e) {
					System.out.println("enter valid   url");

					//e.printStackTrace();
				}
			        Path sourcePath = new Path(localpath);
			        Path destPath = new Path(hdfsDir);
			        try {
						if(!(fs.exists(destPath)))
						{
						    System.out.println("No Such destination exists :"+destPath);
						    return;
						}
					} catch (IOException e) {
						System.out.println("enter valid path");
						//e.printStackTrace();
					}
			         
			        try {
						fs.copyFromLocalFile(sourcePath, destPath);
					} catch (IOException e) {
						System.out.println("enter valid details");
						//e.printStackTrace();
					}
			         
			    
			}

		}
	
