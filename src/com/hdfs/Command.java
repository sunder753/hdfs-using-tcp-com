package com.hdfs;

public interface Command {
	void run() ;
}
