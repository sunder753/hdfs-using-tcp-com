package com.hdfs.ls;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;


public class Ls extends LoadConfigurations implements Command {
 
	@Override
	public void run() {

    System.out.println("enter the direrctory name you want to list");
    String uri=s.next();
    // Get the instance of the HDFS
    FileSystem fs = null;
	try {
		fs = FileSystem.get(new URI(hdfs+uri), conf);
	} catch (Exception e) {
		System.out.println("file not found");
		//e.printStackTrace();
	} 
    
	// Get the metadata of the desired directory
    FileStatus[] fileStatus = null;
	try {
		fileStatus = fs.listStatus(new Path(hdfs+uri));
	} catch (IOException e) {
		System.out.println("please enter a valid file name");
		//e.printStackTrace();
	}
	
    
	// Using FileUtil, getting the Paths for all the FileStatus
    Path[] paths = FileUtil.stat2Paths(fileStatus);
   
    // Iterate through the directory and display the files in it
    System.out.println("Contents of the Directory");
    for(Path path : paths)
    {
      System.out.println(path);
    }
  }
  
}
