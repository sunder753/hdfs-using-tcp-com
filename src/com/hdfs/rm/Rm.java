package com.hdfs.rm;

import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import com.hdfs.Command;
import com.hdfs.LoadConfigurations;

public class Rm extends LoadConfigurations implements Command {

	@Override
	public void run() {
	
		//Enter hdfs uri to delete
		System.out.println("enter hdfs dir along with file to delete");
		String uri=hdfs+s.next();
		
		// Get the instance of the HDFS 
		FileSystem fs = null;
		try {
			fs = FileSystem.get(URI.create(uri),conf);
		} catch (IOException e1) {
			System.out.println("enter valid   url");

			//e1.printStackTrace();
		}
		try {
			fs.delete(new Path(uri),true);
		} catch (IOException e) {
			System.out.println("enter valid details");
			//e.printStackTrace();
		}
		
		
	}

}
